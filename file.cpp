#include "file.hpp"

#include <fstream>

File::File(std::string _path)
: name(""), path(""){
    this->path = fs::path(_path);
    this->name = this->path.filename().string();
    if(!fs::exists(path)){
        std::ofstream file(this->path);
        file.close();
    }
    this->last_modified = fs::last_write_time(this->path);
}

File::File(fs::path _path)
: name(_path.filename().string()), path(_path){
    if(!fs::exists(path)){
        std::ofstream file(this->path);
        file.close();
    }
    this->last_modified = fs::last_write_time(this->path);
}

File::File()
: name(""), path(""){}

File File::load(std::string _path){
    File f(_path);
    return f;
}


void File::write(std::string content, const std::ios_base::openmode mode){
    std::ofstream file(this->path, mode);
    file << content;
    file.close();
}

void File::write(std::string content){
    this->write(content, std::ios::in);
}

void File::append(std::string content){
    this->write(content, std::ios::app);
}

// return the number of lines in the file
unsigned int File::size(){
    std::ifstream f(this->path);
    unsigned int i = 0;
    std::string line;
    while(!f.eof()){
        std::getline(f, line); // read a line
        i++;
    }
    f.close();
    return i;
}

std::string File::read(int start, int end){
    std::string content;
    std::ifstream file(this->path);
    int i = 0;
    while(!file.eof()){
        std::string line;
        std::getline(file, line);
        if(i >= start && i <= end){
            content += line + "\n";
        }
        i++;
    }
    file.close();
    return content;
}

void File::remove(int start, int end){
    std::string content = this->read(0, start - 1) + this->read(end + 1, this->size());
    this->write(content);
}

std::string File::read(){
    return this->read(0, INT_MAX);
}

std::string File::read(int start){
    return this->read(start, INT_MAX);
}