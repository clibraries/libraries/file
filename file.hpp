#ifndef __FILE_HPP__
#define __FILE_HPP__

#include <string>
#include <vector>
#include <filesystem>
namespace fs = std::filesystem;

class File{
    protected:
        std::string name;
        fs::path path;
        fs::file_time_type last_modified;

        void write(std::string content, const std::ios_base::openmode mode);

    public:
        File(std::string path);
        File(fs::path path);
        File();

        inline std::string get_name(){
            return this->name;
        }
        inline std::string get_path(){
            return this->path.string();
        }
        inline fs::file_time_type get_last_modified(){
            return this->last_modified;
        }
        
        File load(std::string path);

        unsigned int size();

        void remove(int start, int end);

        void write(std::string content);
        void append(std::string content);
        std::string read();
        std::string read(int start, int end);
        std::string read(int start);
};

#endif